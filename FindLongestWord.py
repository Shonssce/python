#!/usr/bin/python3
#
# Shon Seet 06/20/2014
#

import sys
from datetime import datetime
import time

"""
Programming Problem - Find Longest Word Made of Other Words

Write a program that reads a file containing a sorted list of words (one word per line, no spaces, all lower case), then identifies the 
	1.	1st longest word in the file that can be constructed by concatenating copies of shorter words also found in the file. 
	2.	The program should then go on to report the 2nd longest word found 
	3.	Total count of how many of the words in the list can be constructed of other words in the list.
"""

word_dictionary = {}			# create a dictionary
length_of_word_list = []	# list of length of all the words
total_match = 0
no_of_longest_words_to_print = 2

def main():
	global length_of_word_list
	
	start_time = time.time()
	# read the data file and build a dictionary
	readFile("wordsforproblem.txt")
	
	# get a list of all the words length in the file
	length_of_word_list = list(word_dictionary.keys())
	
	# get to work...
	startFindWords()
	
	total_time = time.time() - start_time
	print("Time takes to run: " + str(total_time))
	
	


# Read all the words in the file and group them by word length.
# The list of words are sorted.
# eg.([3]=>[aaa, ark, cat, dog, ice])
def readFile(fname):
	# open the data file for reading
	data_file = open(fname, "r")
	# make sure the file is opened
	if data_file.mode == "r":
		# read the entire file
		flist = data_file.readlines()
		for word in flist:
			# for each word, trim space and strip "\n\r" if any
			stripped_word = word.replace('\n','').replace('\r','')
			word_length = len(stripped_word)
			# if valid word
			if (word_length > 0): 
				try:
					# get the existing list
					llist = word_dictionary[word_length]
					# add new word to the list
					llist.append(stripped_word)
				except:
					# new list
					llist = [stripped_word]					
				word_dictionary[word_length] = llist	

#
# Want this to return all the combinations of length of words in dictionary
# Eg: 10 ([8,2],[7,3],[6,4],[5,5],[5,3,2],[4,4,2],[3,3,2,2])
#
def getListOfWordLen(word_length):
	# make sure there is no duplicates
	comb_list = set()
	# initialize return list
	comb_list.add(())
	
	# loop through the word length list, eg. (28, 27, 25, 22,...)
	for wl1 in length_of_word_list:
		# this length should be shorter than word_length, otherwise can't build the combination
		if (wl1 > word_length): 
			# start from word_length 
			wl1 = word_length
			break
		# get the difference
		part_length = word_length - wl1
		# if the difference in length is a length of words on the file
		if (part_length in length_of_word_list):
			# get the next sub word length
			for next_length in getListOfWordLen(part_length):
				if not next_length:
					# this builds (x, y) combination 
					comb_list.add(tuple((wl1, part_length)))
				else:
					# this builds 3 and more combiations
					comb_list.add(tuple((wl1, ) + next_length))	
	return comb_list



# Work on each word with the specified length, 'word_length'
def findAllWords(word_length, print_string):
	# count the total number of words than can be constructed
	global total_match
	
	# get the combination of word length that can be constructed of other words
	comb_list = list(getListOfWordLen(word_length))	
	# need to remove the empty list
	comb_list.remove(())
	# initialize match
	match_word = False	
	# if the word has no possible combination, just return
	# eg. word with length 3, possible combination is (2,1), so
	#     if there isn't any 1 character word in the file, then			
	if len(comb_list) == 0: return match_word
	
	# loop through each word in the list of words with length 'word_length'
	for current_word in word_dictionary[word_length]:
		# loop through each combination,eg. (5,3),(4,4)
		for cl in comb_list:
			# split the word and find a match
			if splitWord(current_word, cl):
				# if word can be constructed from other words, add to the count
				total_match += 1
				match_word = True
				if print_string: 
					print(print_string + "'" + current_word +"'" + " at " + str(word_length) + " letters.")
				# good enough if any one combination matches, continue with the next word 
				break
	return match_word


# Split the word and check whether it can be constructed from other words
# current_wordd		- the word to check, eg. cephalically - 12 characters
# comb_list 		- the combination of length to split the word, eg. (4,3,5)
#						'cephalically' split into 'ceph, ali, cally'
def splitWord(current_word, comb_list):
	# start breaking the string from position 0
	start_pos = 0
	for cl in comb_list:
		end_pos = start_pos + cl
		if not matchWord(cl, current_word[start_pos:end_pos]):
			# if any substring fails to match, just returns no match 
			return False
		start_pos += cl
	# this means all the substring exists, so the word can be constructed from other words
	return True



# Check whether the substring matches any other word of that length
def matchWord(word_length, part_word):
	
	# if substring matches, returns true, otherwise false 
	if part_word in word_dictionary[word_length]:
		return True
	return False
	

def startFindWords():
	# Starts with the longest word in the file
	# Break the words into shorter length words,
	# Check whether the shorter words exist in the file
	# If all the shorter words exists, then the word can be constructed from other words in the file
	# Requirement is to:
	#	- print the longest word that can be constructed from other words in the file
	#	- print the 2nd longest word that can be constructed from other words in the file
	#	- get the number of words in the file that can be constructed from other words in the file
	
	# initialize count - the number of words that can be constructed from other words in the file
	count = 0
	
	# starts from the longest words in the file
	for current_length in reversed(length_of_word_list):

		# identify the words and print them out
		if count < no_of_longest_words_to_print:
			if count == 0:
				what_str = "1st"
			else:
				what_str = "2nd"
			print_str = "The answer for " + what_str + " longest word: "
		else:
			print_str = ""
		
		# start checking for all words with this length
		if findAllWords(current_length, print_str): count += 1
	
	# Finally print the total number of words that can be constructed of other
	# words in the file
	print("Total number of words in the list that can be constructed of other words in the list is: " + str(total_match))
	
	
if __name__ == "__main__": 
	main()
